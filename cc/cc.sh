#!/usr/bin/env bash

# cc for "clear cache"

#https://unix.stackexchange.com/questions/107800/using-while-loop-to-ssh-to-multiple-servers

# secure path
readonly PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# disable alias
\unalias -a

readonly Peer=192.168.0.11
readonly SshOpt="-p65022"
readonly CachePathFile=cachepath.conf
readonly MaintenanceFile=".maintenance.fla"
readonly MaintenancePath=/tmp/toto/1/

readonly ScriptName=$(basename ${0/.sh/})
readonly LockFile=/tmp/${ScriptName}.lock

Help="Usage: ${ScriptName} [OPTION]...\n\n
      DESCRIPTION\n
      ${ScriptName} can clear cache and enable or disable maintenance page.\n\n
      OPTIONS\n
        [1] clear\t drop cache\n
        [2] maint on\t  create an maintenance page\n
        [3] maint off\t remove an maintenance page\n\n
      EXAMPLES\n  
      # clear cache\n
      bash cc.sh clear\n
      # enable maintenance page\n
      bash cc.sh maint on\n
      # disable maintenance page\n
      bash cc.sh maint off\n
      " 

trap Cleanup EXIT SIGINT

# clean exit
function Cleanup() {
   if [ -f $LockFile ] ; then
      rm -f $LockFile
   fi   
}

## check script dependency
for Prog in ssh ; do
   if ! type -ap $Prog &> /dev/null ; then
      echo "error: $Prog not found, exit" ;
      exit 2
   fi
done

PrintDate() {
   date "+%d/%m/%y-%H:%M:%S"
}

DeleteCache() {
   if ! [ -f $CachePathFile ] ; then
	  echo "$CachePathFile does not exist, not cache path !!!"
	  exit 1 
   fi	  
   while read -u10 Line ; do
      if ! echo $Line | grep -qE "^#" ; then
         CheckPeer
         ssh $SshOpt $Peer "if [ -d $Line ] ; then rm -rf $Line  && echo "$Line was delete" ; else echo "$Line does not exist" ; exit 0 ; fi" ;
      fi   
   done 10< $CachePathFile
}

CheckPeer() {
   if ! ssh $SshOpt -q $Peer "exit" ; then
      echo "Unable to open connection to $Peer"
      exit 2
   fi
}

MaintMode() {
   CheckPeer
   case $1 in
      on)
         echo "Enable maintenance page on $MaintenancePath"
         ssh $SshOpt -q $Peer "if [ -d $MaintenancePath ] ; then touch $MaintenancePath$MaintenanceFile ; else echo "$MaintenancePath does not exist" ; fi";
      ;;
      off)
         echo "Disable maintenance page on $MaintenancePath"
         ssh $SshOpt -q $Peer "if [ -f $MaintenancePath$MaintenanceFile ] ; then rm -f $MaintenancePath$MaintenanceFile && echo "$MaintenancePath$PathMaintenanceFile was delete"; else echo "$MaintenancePath$MaintenanceFile does not exist" ; fi" ;
      ;;
   esac   
}

# print output when isn't a cron process
if ! env | grep -q TERM ; then
   exec 1>> /tmp/${ScriptName}.log
   exec 2>> /tmp/${ScriptName}.error
fi 

if [ -z $Peer ] ; then
   echo "Specify a peer name please"
   exit 2
fi

(
   # a single instance only
   flock -n 9 || echo "An instance is already active" exit 1

   case $1 in
      clear)
         echo "Starting at $(PrintDate)"
         DeleteCache
         echo "Stopping at $(PrintDate)"
      ;;   
      maint)
         MaintMode $2
      ;;   
      *)
         echo -e $Help
      ;;   
   esac

) 9>$LockFile
