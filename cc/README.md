# cc

Les variables éditables sont :
* Peer			exemple `192.168.0.11`
* SshOpt		exemple `"-p65022"`
* CachePathFile		exemple `cachepath.conf`
* MaintenanceFile	exemple `".maintenance.html"`
* MaintenancePath	exemple `/tmp/toto/1/`


```
Usage: cc [OPTION]...

 DESCRIPTION
 cc can clear cache and enable or disable maintenance page.

 OPTIONS
 [1] clear       drop cache
 [2] maint on    create an maintenance page
 [3] maint off   remove an maintenance page

 EXAMPLES
 # clear cache
 bash cc.sh clear
 # enable maintenance page
 bash cc.sh maint on
 # disable maintenance page
 bash cc.sh maint off
```
