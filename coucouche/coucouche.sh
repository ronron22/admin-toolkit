#!/bin/bash -

# coucouche a été écrit pour être le plus simple possible d'utilisation.

# pour obtenir la clef d'api, allez
# dans votre User Profile de thruk
# cliquez sur User Profile puis
# dans API Keys, cliquez sur Create New API key

readonly thruk_auth_key=borborygme

address=$1

declare -A svc_hash

svc_suffixe="_NRPE_"

svc_keyv1="alldisks home root tmp var varlib varlog iptables keepalived load memory mysql mysql_slave ntp puppet raid swap totalprocs users zproc mailq ssh http_apache_local apache_certif_local imap imaps ftp http_nginx_local plesk_license pop pops"
svc_valv1=("alldisks" "disk_home" "disk_root" "disk_tmp" "disk_var" "disk_varlib" "disk_varlog" "iptables" "keepalived" "load" "memory" "mysql" "mysql_slave" "ntp" "puppet" "raid" "swap" "totalprocs" "users" "zproc" "mailq" "ssh" "http_apache_local" "apache_certif_local" "imap" "imaps" "ftp" "http_nginx_local" "plesk_license" "pop" "pops")
svc_keyv2="root fpm73 ssh alldisks cron fpm56 fom70 fpm71 fpm72 http https load memory mysql_connection-time mysql_threads-connected ntp nrpe users"
svc_valv2=("check disk root" "check fpm 73" "ssh" "check alldisks" "check cron" "check fpm 56" "check fpm 70" "check fpm 71" "check fpm 72" "check http" "check https" "check load" "check memory" "check mysql connection-time" "check mysql threads-connected" "check ntp" "check tcp nrpe" "check users")

create_svc_hash() {
   counter=0
   if (( version == 1 )) ; then
      for svc in ${svc_keyv1} ; do
         #echo "ksvc : $svc"   
         svc_val=${svc_valv1[$counter]}
         #echo "vsvc : $svc_val"   
         svc_hash["${svc}"]="$address${svc_suffixe}${svc_val}"
         ((counter=counter+1))
         #echo $counter
      done   
   elif (( version == 2 )) ; then
      for svc in ${svc_keyv2} ; do
         #echo "ksvc : $svc"   
         svc_val=${svc_valv2[$counter]}
         #echo "vsvc : $svc_val"   
         svc_hash["${svc}"]="${svc_val// /%20}"
         ((counter=counter+1))
         #echo $counter
      done   
   fi   
   #exit 0
} 

get_display_name() {
   # première méthode
   display_name=$(curl -s -H "X-Thruk-Auth-Key: $thruk_auth_key" \
                       -gk "https://shinken01.admin.net/thruk/r/hosts?address=$address" | jq -r   '.[0] | .display_name')
   version=1
   # display_name est null
   if [[ "null" == $display_name ]] ; then
      # seconde méthode pour msv2
      display_name=$(curl -s -H "X-Thruk-Auth-Key: $thruk_auth_key" \
                          -gk "https://shinken01.admin.net/thruk/r/hosts?name=$address" | jq -r   '.[0] | .display_name')
      version=2
   fi   
   if [ ! -z $whatmisisit ] ; then
      echo "$address est un msv$version"
      exit 0
   fi   
   create_svc_hash
}

case $2 in 
   h)
      time_to_sleep="$3"
      comment="$4"
      get_display_name $address
      curl -d "start_time=now" \
           -d "end_time=+${time_to_sleep}m" \
           -d "comment_data=$comment" \
           -H "X-Thruk-Auth-Key: ${thruk_auth_key}" \
           -gk  "https://shinken01.admin.net/thruk/r/host/$display_name/cmd/schedule_host_downtime"
   ;;
   s)
      service_name="$3"
      time_to_sleep="$4"
      comment="$5"
      get_display_name $address
      svc=${svc_hash[$service_name]}
      curl -d "start_time=now" \
           -d "end_time=+${time_to_sleep}m" \
           -d "comment_data=$comment" \
           -H "X-Thruk-Auth-Key: ${thruk_auth_key}" \
           -gk  "https://shinken01.admin.net/thruk/r/services/$display_name/$svc/cmd/schedule_svc_downtime"
   ;;
   r)
      get_display_name $address
      curl  -s \
            -H "X-Thruk-Auth-Key: ${thruk_auth_key}" \
            -k "https://shinken01.admin.net/thruk/r/hosts/$display_name?columns=display_name,state,host_scheduled_downtime_depth,comments" | jq   
      curl  -s \
            -H "X-Thruk-Auth-Key: ${thruk_auth_key}" \
            -k "https://shinken01.admin.net/thruk/r/hosts/$display_name/services?columns=display_name,state,host_scheduled_downtime_depth,comments" | jq   
   ;;   
   m)
      whatmisisit=true
      get_display_name $address
   ;;   
   *)
      echo "coucouche.sh permet d'appliquer un downtime sur un host ou un service, il peut aussi en afficher l'état et la version MIS."
      echo "* Utilisation"
      echo "   [1] pour un host: bash coucouche \"nom du host\" \"h\" \"durée en minute\" \"commentaire\""   
      echo "     exemple: bash coucouche ns31055 h 5 \"coucouche panier !!\""
      echo "   [2] pour un service: bash coucouche \"nom du host\" \"s\" \"nom simplifié du service\" \"durée en minute\" \"commentaire\""   
      echo "     exemple: bash coucouche ns31055 s load 1 \"coucouche panier\""
      echo "   [3] pour connaître l'état d'un host et de ses services: bash coucouche \"nom du host\" \"r\""   
      echo "     exemple: bash coucouche ns31055 r"
      echo "   [4] pour connaître la version MIS d'un serveur: bash coucouche \"nom du host\" \"m\""   
      echo "     exemple: bash coucouche ns31055 m"
      echo "* Services disponibles"
      echo "   [msv1]  ${svc_keyv1}" 
      echo "   [msv2] ${svc_keyv2}"  
   ;;
esac   
