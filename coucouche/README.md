## coucouche (panier)

coucouche permet de poser des downtimes hosts et services sur l'api thruk.
Il peut aussi afficher les services et leurs états  ainsi que la version MS.

Il a été écrit pour être le plus simple possible d'utilisation.

Attention, il faut penser au fait que les checks *msv1* et *msv2* diffères, lancer `bash coucouche.sh help` pour obtenir la liste simplifiée des services.

Pour obtenir la clef d'API, allez dans votre HOME thruk, cliquez sur User Profile puis dans API Keys, cliquez sur Create New API key


### Aide

```
coucouche.sh permet d'appliquer un downtime sur un host ou un service, il peut aussi en afficher l'état et la version MS.
* Utilisation
   [1] pour un host: bash coucouche "nom du host" "h" "durée en minute" "commentaire"
     exemple: bash coucouche ns31055 h 5 "coucouche panier !!"
   [2] pour un service: bash coucouche "nom du host" "s" "nom simplifié du service" "durée en minute" "commentaire"
     exemple: bash coucouche ns31055 s load 1 "coucouche panier"
   [3] pour connaître l'état d'un host et de ses services: bash coucouche "nom du host" "r"
     exemple: bash coucouche ns31055 r
   [4] pour connaître la version MS d'un serveur: bash coucouche "nom du host" "m"
     exemple: bash coucouche ns31055 m
* Services disponibles
   [msv1]  alldisks home root tmp var varlib varlog iptables keepalived load memory mysql mysql_slave ntp puppet raid swap totalprocs users zproc mailq ssh http_apache_local apache_certif_local imap imaps ftp http_nginx_local plesk_license pop pops
   [msv2] root fpm73 ssh alldisks cron fpm56 fom70 fpm71 fpm72 http https load memory mysql_connection-time mysql_threads-connected ntp nrpe users
```
